/**
 * Available OAuth 2.0 scopes for use with the Calendar API.
 */
public class CalendarScopes {
/** See, edit, share, and permanently delete all the calendars you can access using Google Calendar. */
  public static final String CALENDAR = 'https://www.googleapis.com/auth/calendar';

  /** View and edit events on all your calendars. */
  public static final String CALENDAR_EVENTS = 'https://www.googleapis.com/auth/calendar.events';

  /** View events on all your calendars. */
  public static final String CALENDAR_EVENTS_READONLY = 'https://www.googleapis.com/auth/calendar.events.readonly';

  /** View your calendars. */
  public static final String CALENDAR_READONLY = 'https://www.googleapis.com/auth/calendar.readonly';

  /** View your Calendar settings. */
  public static final String CALENDAR_SETTINGS_READONLY = 'https://www.googleapis.com/auth/calendar.settings.readonly';

  /**
   * Returns an unmodifiable set that contains all scopes declared by this class.
   *
   * @since 1.16
   */
  public static Set<String> all() {
    Set<String> result = new Set<String>();
    result.add(CALENDAR);
    result.add(CALENDAR_EVENTS);
    result.add(CALENDAR_EVENTS_READONLY);
    result.add(CALENDAR_READONLY);
    result.add(CALENDAR_SETTINGS_READONLY);
    return result;
  }

  private CalendarScopes() {
  }
}