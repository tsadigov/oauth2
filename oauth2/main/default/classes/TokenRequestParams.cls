public class TokenRequestParams extends GenericData{
    public static TokenRequestParams createToken(string code){
        Map<String,Object> m=(Map<String,Object> )JSON.deserializeUntyped(GoogleOAuthConstants.credentials);
        m=(Map<String,Object> )m.get('web');
                
        TokenRequestParams p= new TokenRequestParams('authorization_code');
        
        p.client_id=(string)m.get('client_id');
        
        p.redirect_uri=OAuthHelper.RedirectUri;
        p.code=code;
        p.client_secret=(String)m.get('client_secret');
        return p;
    }
    
    public static TokenRequestParams createRefresh(string refreshToken){
        Map<String,Object> m=(Map<String,Object> )JSON.deserializeUntyped(GoogleOAuthConstants.credentials);
        m=(Map<String,Object> )m.get('web');
        
        TokenRequestParams p = new TokenRequestParams('refresh_token');
        
        p.client_id=(string)m.get('client_id');
        p.client_secret=(String)m.get('client_secret');
        
        p.put('refresh_token', refreshToken);
        return p;
    }
    
    public TokenRequestParams(string gt){
        this.grant_type=gt;
    }
    
    
    public string code
    {get{return (String)this.get('code');}set{this.put('code',value);}}
    public string client_id
    {get{return (String)this.get('client_id');}set{this.put('client_id',value);}}
    public string redirect_uri
    {get{return (String)this.get('redirect_uri');}set{this.put('redirect_uri',value);}}
    public string grant_type
    {get{return (String)this.get('grant_type');}set{this.put('grant_type',value);}}
    public string client_secret
    {get{return (String)this.get('client_secret');}set{this.put('client_secret',value);}}
}