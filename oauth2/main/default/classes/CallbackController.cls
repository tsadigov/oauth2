/**
 *  Callback for google to notify about the result
 *
 * @author Tural SADIK
 */
 
 public without sharing class CallbackController {
    public string accessToken{get;set;}
    public string code{get;set;}
    public string state{get;set;}
    public string u{get;set;}
    public string body{get;set;}
    public string Message{get;set;}
    
    public void defaultAction(){
        this.code=ApexPages.currentPage().getParameters().get('code');
        this.state=ApexPages.currentPage().getParameters().get('state');
        
        TokenResponse tkn=OAuthHelper.tokenFromAuthCode(code);
        System.debug(tkn);
        if(tkn.IsOk){            
            Contact theContact=[SELECT Id, CalendarCredentials__c FROM Contact WHERE CalendarRequest__c =:this.state];
            theContact.CalendarCredentials__c=JSON.serialize(tkn);
            
            update theContact;
        }
        else{
            this.Message=tkn.error;
        }
        
        /*
            if (res.data && res.data.expires_in) {
                tokens.expiry_date =
                    ((new Date()).getTime() + (res.data.expires_in * 1000));
                delete tokens.expires_in;
            }
            this.emit('tokens', tokens);
            return { tokens, res };
        
        */
        
    }
}