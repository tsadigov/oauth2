/**
 * Constants for Google's OAuth 2.0 implementation.
 *
 * @author Tural SADIK
 */
public class GoogleOAuthConstants {
    
    //TODO: extract to custom setting admin setup and for reusable package
    public static string credentials='{'+
    '"web": {'+
        '"client_id": "64673199310-87ejh1rnb2ro4h5sop5pi1ijoomr9p19.apps.googleusercontent.com",'+
        '"project_id": "quickstart-1576774081486",'+
        '"auth_uri": "https://accounts.google.com/o/oauth2/auth",'+
        '"token_uri": "https://oauth2.googleapis.com/token",'+
        '"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",'+
        '"client_secret": "KqMWBK_AKtMx0820uaXrEthE",'+
        '"redirect_uris": ['+
            '"https://c.na174.visual.force.com/apex/CallbackPage",'+
            '"https://app-app-9156-dev-ed--c.visualforce.com/apex/callbackpage",'+
            '"https://power-innovation-9076-dev-ed--c.visualforce.com/apex/callbackpage"'+
        '],'+
        '"javascript_origins": ['+
            '"http://localhost:8000",'+
            '"http://0.0.0.0:8000"'+
        ']'+
    '}'+
'}';

  /** Encoded URL of Google's end-user authorization server. */
  public static final String AUTHORIZATION_SERVER_URL = 'https://accounts.google.com/o/oauth2/auth';

  /** Encoded URL of Google's token server. */
  public static final String TOKEN_SERVER_URL = 'https://accounts.google.com/o/oauth2/token';

  /**
   * Encoded URL of Google's public certificates.
   */
  public static final String DEFAULT_PUBLIC_CERTS_ENCODED_URL =
      'https://www.googleapis.com/oauth2/v1/certs';

  public static final String REVOKE_URL='https://accounts.google.com/o/oauth2/revoke';

  /**
   * Redirect URI to use for an installed application as specified in <a
   * href="http://code.google.com/apis/accounts/docs/OAuth2InstalledApp.html">Using OAuth 2.0 for
   * Installed Applications</a>.
   */
  public static final String OOB_REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob';

  private GoogleOAuthConstants() {
  }
}