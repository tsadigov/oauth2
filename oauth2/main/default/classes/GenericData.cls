/**
 * Generic data that stores all unknown data key name/value pairs.
 *
 * <p>
 * Subclasses can declare fields for known data keys using the {@link Key} annotation. Each field
 * can be of any visibility (private, package private, protected, or public) and must not be static.
 * {@code null} unknown data key names are not allowed, but {@code null} data values are allowed.
 * </p>
 *
 * <p>
 * Iteration order of the data keys is based on the sorted (ascending) key names of the declared
 * fields, followed by the iteration order of all of the unknown data key name/value pairs.
 * </p>
 *
 * <p>
 * Implementation is not thread-safe. For a thread-safe choice instead use an implementation of
 * {@link ConcurrentMap}.
 * </p>
 *
 * @since 1.0
 * @author Yaniv Inbar
 */
public virtual class GenericData //extends Map<String, Object>  
{

  /** Map of unknown fields. */
  Map<String, Object> unknownFields = new Map<String, Object>();

  protected Map<String, Object> m_FieldValues = new Map<String, Object>();

    
  // TODO(yanivi): implement more methods for faster implementation

  /** Class information. */
  //final ClassInfo classInfo;

  /**
   * Constructs with case-insensitive keys.
   */
  public GenericData() {
    //this(EnumSet.noneOf(Flags.class));
  }

  /**
   * Flags that impact behavior of generic data.
   * @since 1.10
   */
  public enum Flags {

    /** Whether keys are case sensitive. */
    IGNORE_CASE
  }

  /**
   * @param flags flags that impact behavior of generic data
   * @since 1.10
   */
  /*
   public GenericData(EnumSet<Flags> flags) {
    classInfo = ClassInfo.of(getClass(), flags.contains(Flags.IGNORE_CASE));
  }
*/

  //@Override
  public virtual Object get(string name) {
      return this.m_FieldValues.get(name);
  }

  //@Override
  public virtual Object put(String fieldName, Object value) {
    return this.m_FieldValues.put(fieldName, value);
  }

  /**
   * Sets the given field value (may be {@code null}) for the given field name. Any existing value
   * for the field will be overwritten. It may be more slightly more efficient than
   * {@link #put(String, Object)} because it avoids accessing the field's original value.
   *
   * <p>
   * Overriding is only supported for the purpose of calling the super implementation and changing
   * the return type, but nothing else.
   * </p>
   */
  public  GenericData setValue(String fieldName, Object value) {
    this.m_FieldValues.put(fieldName,value);
    return this;
  }

  //@Override
  public void putAll(Map< String, Object> src) {      
    /*
    for (Map.Entry<string, Object> entry : srv.entrySet()) {
      set(entry.getKey(), entry.getValue());
    }
      */
      for(String key: src.keyset()){
          this.m_FieldValues.put(key,src.get(key));
      }
  }
    
    
    public Map< String, Object> getAll() {      
        return this.m_FieldValues;
  }

  //@Override
  public Object remove(string name) {
    this.m_FieldValues.remove(name);
      return this;
  }

  //@Override
/*
  public Set<Map.Entry<String, Object>> entrySet() {
    return new EntrySet();
  }
*/
  /**
   * Makes a "deep" clone of the generic data, in which the clone is completely independent of the
   * original.
   */
  //@Override
  /*
  public GenericData clone() {
    try {
      //@SuppressWarnings('unchecked')
      
      GenericData result = (GenericData) super.clone();
      Data.deepCopy(this, result);
      result.unknownFields = Data.clone(unknownFields);
      return result;

        return null;
    } catch (Exception e){//(CloneNotSupportedException e) {
      throw e;
    }
  }
*/

  /**
   * Returns the map of unknown data key name to value.
   *
   * @since 1.5
   */
  public  Map<String, Object> getUnknownKeys() {
    return unknownFields;
  }

  /**
   * Sets the map of unknown data key name to value.
   *
   * @since 1.5
   */
  public  void setUnknownKeys(Map<String, Object> unknownFields) {
    this.unknownFields = unknownFields;
  }

  /**
   * Returns the class information.
   *
   * @since 1.10
   */
    /*
  public final ClassInfo getClassInfo() {
    return classInfo;
  }
*/
    

  /** Set of object data key/value map entries. */
    /*
  final class EntrySet extends AbstractSet<Map.Entry<String, Object>> {

    private final DataMap.EntrySet dataEntrySet;

    EntrySet() {
      dataEntrySet = new DataMap(GenericData.this, classInfo.getIgnoreCase()).entrySet();
    }

    //@Override
    public Iterator<Map.Entry<String, Object>> iterator() {
      return new EntryIterator(dataEntrySet);
    }

    //@Override
    public int size() {
      return unknownFields.size() + dataEntrySet.size();
    }

    //@Override
    public void clear() {
      unknownFields.clear();
      dataEntrySet.clear();
    }
  }
*/
  /**
   * Iterator over the object data key/value map entries which iterates first over the fields and
   * then over the unknown keys.
   */
  
    /*
    final class EntryIterator implements Iterator<Map.Entry<String, Object>> {

    // Whether we've started iterating over the unknown keys. 
    private boolean startedUnknown;

    // Iterator over the fields. 
    private final Iterator<Map.Entry<String, Object>> fieldIterator;

    /** Iterator over the unknown keys. 
    private final Iterator<Map.Entry<String, Object>> unknownIterator;

    EntryIterator(DataMap.EntrySet dataEntrySet) {
      fieldIterator = dataEntrySet.iterator();
      unknownIterator = unknownFields.entrySet().iterator();
    }

    public boolean hasNext() {
      return fieldIterator.hasNext() || unknownIterator.hasNext();
    }

    public Map.Entry<String, Object> next() {
      if (!startedUnknown) {
        if (fieldIterator.hasNext()) {
          return fieldIterator.next();
        }
        startedUnknown = true;
      }
      return unknownIterator.next();
    }

    public void remove() {
      if (startedUnknown) {
        unknownIterator.remove();
      }
      fieldIterator.remove();
    }


  }
*/
}