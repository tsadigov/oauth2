/**
 * OAuth 2.0 URL builder for an authorization web page to allow the end user to authorize the
 * application to access their protected resources and that returns an authorization code, as
 * specified in <a href="http://tools.ietf.org/html/rfc6749#section-4.1">Authorization Code
 * Grant</a>.
 *
 * <p>
 * The default for {@link #getResponseTypes()} is {@code "code"}. Use
 * {@link AuthorizationCodeResponseUrl} to parse the redirect response after the end user
 * grants/denies the request. Using the authorization code in this response, use
 * {@link AuthorizationCodeTokenRequest} to request the access token.
 * </p>
 *
 * <p>
 * Sample usage for a web application:
 * </p>
 *
 * <pre>
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String url =
        new AuthorizationCodeRequestUrl("https://server.example.com/authorize", "s6BhdRkqt3")
            .setState("xyz").setRedirectUri("https://client.example.com/rd").build();
    response.sendRedirect(url);
  }
 * </pre>
 *
 *
 * @author Tural SADIK
 */
public class AuthorizationCodeRequestUrl extends AuthorizationRequestUrl {
	public override void  setValuesFrom(Map<String,String> parameters){
        super.setValuesFrom(parameters);
    }
    
  /**
   * @param authorizationServerEncodedUrl authorization server encoded URL
   * @param clientId client identifier
   */
  public AuthorizationCodeRequestUrl(String authorizationServerEncodedUrl, String clientId) {
    super(authorizationServerEncodedUrl, clientId, Helper.singleton('code'));
  }
}