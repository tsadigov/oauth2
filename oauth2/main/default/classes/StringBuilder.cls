/* ============================================================
 * This code is part of the "apex-lang" open source project avaiable at:
 * 
 *      http://code.google.com/p/apex-lang/
 *
 * This code is licensed under the Apache License, Version 2.0.  You may obtain a 
 * copy of the License at:
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * ============================================================
 */
global class StringBuilder {

    private String theString;
    
    global StringBuilder(){
        this('');
    }
    
    global StringBuilder(String str){
        theString = str;
    }

    global StringBuilder(Decimal d){
        theString = '' + d;
    }
    
    global StringBuilder(Double d){
        theString = '' + d;
    }
    
    global StringBuilder(Long l){
        theString = '' + l;
    }
    
    global StringBuilder(Integer i){
        theString = '' + i;
    }
    
    global StringBuilder(Blob b){
        theString = '' + b;
    }
    
    global StringBuilder(Boolean b){
        theString = '' + b;
    }
    
    global StringBuilder(Date d){
        theString = '' + d;
    }
    
    global StringBuilder(Datetime d){
        theString = '' + d;
    }
    
    global StringBuilder(ID id){
        theString = '' + id;
    }
    
    global StringBuilder(Time t){
        theString = '' + t;
    }

    global StringBuilder append(String str){
        theString += str; return this;    
    }

    global StringBuilder append(Decimal d){
        theString += d;    return this;
    }
    
    global StringBuilder append(Double d){
        theString += d;    return this;
    }
    
    global StringBuilder append(Long l){
        theString += l;    return this;
    }
    
    global StringBuilder append(Integer i){
        theString += i;    return this;
    }
    
    global StringBuilder append(Blob b){
        theString += b;    return this;
    }
    
    global StringBuilder append(Boolean b){
        theString += b;    return this;
    }
    
    global StringBuilder append(Date d){
        theString += d;    return this;
    }
    
    global StringBuilder append(Datetime d){
        theString += d;    return this;
    }
    
    global StringBuilder append(ID id){
        theString += id; return this;
    }
    
    global StringBuilder append(Time t){
        theString += t;    return this;
    }

    public override String toString(){
        return this.toStr();
    }
    
    global String toStr(){
        return theString;
    }
}