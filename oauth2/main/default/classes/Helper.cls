public class Helper {

    private static List<SObject> s_delayedUpdateList=new List<SObject>();
    public static void delayedUpdate(SObject item){
        s_delayedUpdateList.Add(item);
    }
    public static List<SObject> executeDelayedUpdates(){
        update s_delayedUpdateList;
        return s_delayedUpdateList;
    }

    @future(callout=true)
    public static  void asyncUpdate(String id,String sObjectName,Map<String,String> changes){
        String soql='SELECT Id FROM '+sObjectName+' WHERE Id=\''+id+'\'';
        System.debug(soql);
        List<SObject> o=Database.query(soql);
        if(1==o.size())
            for(String key:changes.keySet())
                o[0].put(key, changes.get(key));
        update o;
    }
    
    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
    
    public static Map<String,Object> Only(Map<String,Object> src,List<String> keyset){
        Map<String,Object> result=new Map<String,Object>();
        for(String k:keyset)
            result.put(k,src.get(k));
        return result;
    }
    public static String Join(List<String> parts,String separator){
    	String result='';
        String sep='';
        if(parts.size()<2)separator='';
        
        for(String part : parts){
            result+=sep+part; 
            sep=separator;
        }
        return result;
    }
    
    public static List<String> singleton(String val){
    	List<String> code=new List<String>();
    	code.Add(val);
        return code;
    }
    
    public static String urlParameters(Map<String,Object> parameters){
        StringBuilder sb=new StringBuilder();
        String seperator='';
        for(String k:parameters.keySet()){
            if(null==parameters.get(k))continue;
            sb.append(seperator+k+'='+String.valueof(parameters.get(k)));
            seperator='&';
        }
        return sb.ToStr();
    }
    
}