public class OAuthHelper {
    
    public static TokenResponse tokenFromAuthCode(string code){
        TokenRequestParams p=TokenRequestParams.createToken(code);
        return fetchResponse(p);
    }
    
    public static TokenResponse tokenForContact(Contact theContact){
        String jsonToken=theContact.CalendarCredentials__c;
        if(String.isBlank(jsonToken))
            return null;

        TokenResponse tkn = (TokenResponse) JSON.deserialize(jsonToken,TokenResponse.class);
        boolean refreshed = refreshIfExpired(tkn);

        if(refreshed){
            theContact.CalendarCredentials__c=JSON.serialize(tkn);
            Helper.delayedUpdate(theContact);
        }
        return tkn;
    }

    public static TokenResponse tokenForContact(Id id){
        List<Contact> contacts=[SELECT Id, Name, CalendarRequest__c, CalendarCredentials__c FROM Contact WHERE Id=:Id LIMIT 1];
        if(0==contacts.size())
            return null;
        
        Contact theContact=contacts[0];
        return tokenForContact(theContact);
    }

    public static TokenResponse tokenFromRefresh(string refreshToken){
        TokenRequestParams p=TokenRequestParams.createRefresh(refreshToken);        
        return fetchResponse(p);
    }
    
    public static boolean hasExpired(TokenResponse existing){
        boolean result = System.DateTime.Now().getTime()>existing.expires_in;
        system.debug('has expired '+result+'   time:'+System.DateTime.Now().getTime()+'  expires_in:'+existing.expires_in);
        return result;
    }
    
    public static boolean refreshIfExpired(TokenResponse existing){
        if(hasExpired(existing)){
            TokenResponse refreshed=tokenFromRefresh(existing.refresh_token);
            if(refreshed.IsOk){
                existing.expires_in=refreshed.expires_in;
                existing.access_token=refreshed.access_token;
                return true;
            }
            else {
                throw new CalloutException(refreshed.error);
            }
        }
        System.debug('token is still valid no need to refresh');
        return false;
    }
    
    static TokenResponse fetchResponse(TokenRequestParams p){
        String urlParams=Helper.urlParameters(p.getAll());
        
        HttpRequest req = new HttpRequest();
     	req.setEndpoint(GoogleOAuthConstants.TOKEN_SERVER_URL);
	    req.setMethod('POST');

        req.setBody(urlParams);
        
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded' );
     
        Http http = new Http();
        HTTPResponse res = http.send(req);
            
        String responseJsonString=res.getBody().unescapeHtml4();
        TokenResponse tkn=(TokenResponse)JSON.deserialize(responseJsonString, TokenResponse.class);
        if(String.isNotBlank(tkn.access_token)){
            tkn.expires_in= System.DateTime.Now().getTime()+tkn.expires_in*1000;//minus buffer
        }
        return tkn;
    }

    public static boolean revokeAccess(TokenResponse tkn){
        String urlParams=Helper.urlParameters(new Map<String, String>{'token'=>tkn.access_token});
        
        HttpRequest req = new HttpRequest();
     	req.setEndpoint(GoogleOAuthConstants.REVOKE_URL);
	    req.setMethod('POST');

        req.setBody(urlParams);
        
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded' );
     
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return 200 == res.getStatusCode();
    }

    public static string RedirectUri{
        get{
            string BaseURL = ApexPages.currentPage().getHeaders().get('Host');
            return 'https://'+BaseURL+Page.CallbackPage.getUrl();
        }
    }
    
    public static String requestPermissionUrl(string state){
        Map<String,Object> m=(Map<String,Object> )JSON.deserializeUntyped(GoogleOAuthConstants.credentials);
        m=(Map<String,Object> )m.get('web');
        
        AuthorizationCodeRequestUrl u=new AuthorizationCodeRequestUrl (GoogleOAuthConstants.AUTHORIZATION_SERVER_URL, (String)m.get('client_id'));
        
        u.setScopes(new List<String>(CalendarScopes.all()));
        u.accessType='offline';        
        
        u.setRedirectUri(OAuthHelper.RedirectUri);
        u.put('state',state);
        
        return u.build();
    }
}