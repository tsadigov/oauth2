/**
 * OAuth 2.0 URL builder for an authorization web page to allow the end user to authorize the
 * application to access their protected resources, as specified in <a
 * href="http://tools.ietf.org/html/rfc6749#section-3.1">Authorization Endpoint</a>.
 *
 * <p>
 * Sample usage for a web application:
 * </p>
 *
 * <pre>
  public void doGet(HttpServletRequest request, HttpServletResponse response)  {
    String url = new AuthorizationRequestUrl(
        "https://server.example.com/authorize", "s6BhdRkqt3", Arrays.asList("code")).setState("xyz")
        .setRedirectUri("https://client.example.com/rd").build();
    response.sendRedirect(url);
  }
 * </pre>
 *
 * @author Tural SADIK
 */
public virtual class AuthorizationRequestUrl extends GenericUrl {
    public virtual override void  setValuesFrom(Map<String,String> parameters){
        super.setValuesFrom(parameters);
    }

  /**
   * @param authorizationServerEncodedUrl authorization server encoded URL
   * @param clientId client identifier
   * @param responseTypes <a href="http://tools.ietf.org/html/rfc6749#section-3.1.1">response
   *        type</a>, which must be for requesting an authorization code,
   *        for requesting an access token (implicit grant), or a list of registered
   *        extension values to join with a space
   */
  public AuthorizationRequestUrl(
      String authorizationServerEncodedUrl, String clientId, List<String> responseTypes) {
    super(authorizationServerEncodedUrl);
    setClientId(clientId);
    setResponseTypes(responseTypes);
  }
        
  public String accessType{
      get{
          return String.valueOf(this.get('access_type'));
      }
      set{
          this.put('access_type',value);
      }
  }
    
    
  /**
   * <a href="http://tools.ietf.org/html/rfc6749#section-3.1.1">Response type</a>, which must be
   * for requesting an authorization code, for requesting an access
   * token (implicit grant), or space-separated registered extension values.
   */
  private String responseTypes{
      get{
          return String.valueOf(this.get('response_type'));
      }
      set{
          this.put('response_type',value);
      }
  }



  /**
   * Returns the <a href="http://tools.ietf.org/html/rfc6749#section-3.1.1">Response type</a>, which
   * must be for requesting an authorization code, for requesting an
   * access token (implicit grant), or space-separated registered extension values.
   */
  public String getResponseTypes() {
    return responseTypes;
  }

  /**
   * Sets the <a href="http://tools.ietf.org/html/rfc6749#section-3.1.1">response type</a>, which
   * must be {@code "code"} for requesting an authorization code, {@code "token"} for requesting an
   * access token (implicit grant), or a list of registered extension values to join with a space.
   *
   * <p>
   * Overriding is only supported for the purpose of calling the super implementation and changing
   * the return type, but nothing else.
   * </p>
   *
   */
  public AuthorizationRequestUrl setResponseTypes(List<String> responseTypes) {
    this.responseTypes = Helper.Join(responseTypes,' ');
    return this;
  }

    
      /**
   * URI that the authorization server directs the resource owner's user-agent back to the client
   * after a successful authorization grant (as specified in <a
   * href="http://tools.ietf.org/html/rfc6749#section-3.1.2">Redirection Endpoint</a>) or
   * {@code null} for none.
   */
    public String redirectUri{
        get{return String.valueOf(this.get('redirect_uri'));}
        set{this.put('redirect_uri',value);}
    }
  /**
   * Returns the URI that the authorization server directs the resource owner's user-agent back to
   * the client after a successful authorization grant (as specified in <a
   * href="http://tools.ietf.org/html/rfc6749#section-3.1.2">Redirection Endpoint</a>) or
   * {@code null} for none.
   */
  public String getRedirectUri() {
    return redirectUri;
  }

  /**
   * Sets the URI that the authorization server directs the resource owner's user-agent back to the
   * client after a successful authorization grant (as specified in <a
   * href="http://tools.ietf.org/html/rfc6749#section-3.1.2">Redirection Endpoint</a>) or
   * {@code null} for none.
   *
   */
  public AuthorizationRequestUrl setRedirectUri(String redirectUri) {
    this.redirectUri = redirectUri;
    return this;
  }

    /**
   * Space-separated list of scopes (as specified in <a
   * href="http://tools.ietf.org/html/rfc6749#section-3.3">Access Token Scope</a>) or {@code null}
   * for none.
   */
    public String scopes{
        get{return String.valueOf(this.get('scope'));}
        set{this.put('scope',value);}
    }
    
  /**
   * Returns the space-separated list of scopes (as specified in <a
   * href="http://tools.ietf.org/html/rfc6749#section-3.3">Access Token Scope</a>) or {@code null}
   * for none.
   */
  public  String getScopes() {
    return scopes;
  }

  /**
   * Sets the list of scopes (as specified in <a
   * href="http://tools.ietf.org/html/rfc6749#section-3.3">Access Token Scope</a>) or {@code null}
   * for none.
   *
   * @param scopes collection of scopes to be joined by a space separator (or a single value
   *        containing multiple space-separated scopes) or {@code null} for none
   */
  public AuthorizationRequestUrl setScopes(List<String> scopes) {
    this.scopes =
        scopes == null || 0==scopes.size()
        ? null 
        : Helper.Join(scopes,' ');
    return this;
  }


  /** Client identifier. */
  private String clientId{
      get{return String.valueOf(this.get('client_id'));}
      set{this.put('client_id',value);}
  }

  /** Returns the client identifier. */
  public  String getClientId() {
    return clientId;
  }

  /**
   * Sets the client identifier.
   *
   * <p>
   * Overriding is only supported for the purpose of calling the super implementation and changing
   * the return type, but nothing else.
   * </p>
   */
  public AuthorizationRequestUrl setClientId(String clientId) {
    this.clientId = /*Preconditions.checkNotNull*/(clientId);
    return this;
  }

  /**
   * State (an opaque value used by the client to maintain state between the request and callback,
   * as mentioned in <a href="http://tools.ietf.org/html/rfc6749#section-3.1.2.2">Registration
   * Requirements</a>) or {@code null} for none.
   */
  private String state{
      get{return String.valueOf(this.get('state'));}
      set{this.put('state',value);}
  }
  /**
   * Returns the state (an opaque value used by the client to maintain state between the request and
   * callback, as mentioned in <a
   * href="http://tools.ietf.org/html/rfc6749#section-3.1.2.2">Registration Requirements</a>) or
   * {@code null} for none.
   */
  public  String getState() {
    return state;
  }

  /**
   * Sets the state (an opaque value used by the client to maintain state between the request and
   * callback, as mentioned in <a
   * href="http://tools.ietf.org/html/rfc6749#section-3.1.2.2">Registration Requirements</a>) or
   * {@code null} for none.
   *
   * <p>
   * Overriding is only supported for the purpose of calling the super implementation and changing
   * the return type, but nothing else.
   * </p>
   */
  public AuthorizationRequestUrl setState(String state) {
    this.state = state;
    return this;
  }

  
}