public without sharing class CalendarPermissionController {

    public String email{get;set;}
    public string address{get;set;}
    public string Message{get;set;}

    public void defaultAction(){
        
        this.email=ApexPages.currentPage().getParameters().get('email');
        
        List<Contact> contacts=[SELECT Id,Name, Email,CalendarRequest__c,CalendarCredentials__c FROM Contact WHERE email=:email LIMIT 1];
        
        if(0==contacts.size()){
            this.Message='Could not find invidation. Possibly expired invitation, please request new one';
        }
        else{
            Contact theContact=contacts[0];
            if(String.isNotBlank(theContact.CalendarCredentials__c)){
                TokenResponse tkn = (TokenResponse) JSON.deserialize(theContact.CalendarCredentials__c,TokenResponse.class);
                OAuthHelper.revokeAccess(tkn);
                theContact.CalendarCredentials__c=null;
            }
            theContact.CalendarRequest__c = Helper.generateRandomString(Contact.Contact.CalendarRequest__c.getDescribe().getLength());
            update theContact;
            
            this.address=OAuthHelper.requestPermissionUrl(theContact.CalendarRequest__c);
        }
    }
    
}