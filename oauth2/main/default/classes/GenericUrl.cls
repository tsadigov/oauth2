/**
 * URL builder in which the query parameters are specified as generic data key/value pairs, based on
 * the specification <a href="http://tools.ietf.org/html/rfc3986">RFC 3986: Uniform Resource
 * Identifier (URI)</a>.
 *
 * <p>
 * The query parameters are specified with the data key name as the parameter name, and the data
 * value as the parameter value. Subclasses can declare fields for known query parameters using the
 * {@link Key} annotation. {@code null} parameter names are not allowed, but {@code null} query
 * values are allowed.
 * </p>
 *
 * <p>
 * Query parameter values are parsed using {@link UrlEncodedParser#parse(String, Object)}.
 * </p>
 *
 * @author Tural SADIK
 */
public virtual class GenericUrl extends GenericData {
    
    public void AppendAllParameters(Map<String,string> params){
        for(String k:params.keySet()){
            this.m_FieldValues.put(k,params.get(k));
        }
    } 

  //private static final Escaper URI_FRAGMENT_ESCAPER =
      //new PercentEscaper('=&-_.!~*\'()@:$,;/?:', false);

  /** Scheme (lowercase), for example {@code "https"}. */
  private String scheme;

  /** Host, for example {@code "www.google.com"}. */
  private String host;

  /** User info or {@code null} for none, for example {@code "username:password"}. */
  private String userInfo;

  /** Port number or {@code -1} if undefined, for example {@code 443}. */
  private integer port = -1;

  /**
   * Decoded path component by parts with each part separated by a {@code '/'} or {@code null} for
   * none, for example {@code "/m8/feeds/contacts/default/full"} is represented by {@code "", "m8",
   *"feeds", "contacts", "default", "full"}.
   * <p>
   * Use {@link #appendRawPath(String)} to append to the path, which ensures that no extra slash is
   * added.
   * </p>
   */
  private List<String> pathParts;

  /** Fragment component or {@code null} for none. */
  private String fragment;

  public GenericUrl() {
  }

  /**
   * Constructs from an encoded URL.
   *
   * <p>
   * Any known query parameters with pre-defined fields as data keys will be parsed based on their
   * data type. Any unrecognized query parameter will always be parsed as a string.
   * </p>
   *
   * <p>
   * Any {@link MalformedURLException} is wrapped in an {@link IllegalArgumentException}.
   * </p>
   *
   * <p>Upgrade warning: starting in version 1.18 this parses the encodedUrl using
   * new URL(encodedUrl). In previous versions it used new URI(encodedUrl).
   * In particular, this means that only a limited set of schemes are allowed such as "http" and
   * "https", but that parsing is compliant with, at least, RFC 3986.</p>
   *
   * @param encodedUrl encoded URL, including any existing query parameters that should be parsed
   * @throws IllegalArgumentException if URL has a syntax error
   */
  public GenericUrl(String encodedUrl) {
    this(parseURL(encodedUrl));
  }

  /**
   * Constructs from a URI.
   *
   * @param uri URI
   *
   * @since 1.14
   */
    /*
  public GenericUrl(URI uri) {
    this(uri.getScheme(),
        uri.getHost(),
        uri.getPort(),
        uri.getRawPath(),
        uri.getRawFragment(),
        uri.getRawQuery(),
        uri.getRawUserInfo());
  }
*/
    
  /**
   * Constructs from a URL.
   *
   * @param url URL
   */
  public GenericUrl(URL url) {
    this(
        url.getProtocol(),
        url.getHost(),
        url.getPort(),
        url.getPath(),
        url.getRef(),
        url.getQuery(),
        url.getUserInfo());
  }
    
    public virtual void  setValuesFrom(Map<String,String> parameters){
        
    }

  private GenericUrl(
      String scheme,
      String host,
      integer port,
      String path,
      String fragment,
      String query,
      String userInfo) {
    this.scheme = scheme.toLowerCase();
    this.host = host;
    this.port = port;
    this.pathParts = toPathParts(path);
    this.fragment = fragment != null ? /*CharEscapers.decodeUri*/(fragment) : null;
    if (query != null) {
      System.Url t_url=new System.Url(scheme,host,port,path+query);
      System.PageReference ref=new System.PageReference(t_url.toExternalForm());
      Map<String,String> parameters=ref.getParameters(); 
      setValuesFrom(parameters);
      //UrlEncodedParser.parse(query, this);
    }
    this.userInfo = userInfo != null ? /*CharEscapers.decodeUri*/(userInfo) : null;
  }

  public integer hashCode() {
    // TODO(yanivi): optimize?
    return build().hashCode();
  }

  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj) || !(obj instanceof GenericUrl)) {
      return false;
    }
    GenericUrl other = (GenericUrl) obj;
    return build().equals(other.toString());
  }

  public override String toString() {
    return build();
  }

  /**
   * Returns the scheme (lowercase), for example {@code "https"}.
   */
  public  String getScheme() {
    return scheme;
  }

  /**
   * Sets the scheme (lowercase), for example {@code "https"}.
   */
  public  void setScheme(String scheme) {
    this.scheme = /*Preconditions.checkNotNull*/(scheme);
  }

  /**
   * Returns the host, for example {@code "www.google.com"}.
   */
  public String getHost() {
    return host;
  }

  /**
   * Sets the host, for example {@code "www.google.com"}.
   */
  public  void setHost(String host) {
    this.host = /*Preconditions.checkNotNull*/(host);
  }

  /**
   * Returns the user info or {@code null} for none, for example {@code "username:password"}.
   */
  public String getUserInfo() {
    return userInfo;
  }

  /**
   * Sets the user info or {@code null} for none, for example {@code "username:password"}.
   */
  public void setUserInfo(String userInfo) {
    this.userInfo = userInfo;
  }

  /**
   * Returns the port number or {@code -1} if undefined, for example {@code 443}.
   */
  public integer getPort() {
    return port;
  }

  /**
   * Sets the port number, for example {@code 443}.
   */
  public void setPort(integer port) {
    //Preconditions.checkArgument(port >= -1, 'expected port >= -1');
    this.port = port;
  }

  /**
   * Returns the decoded path component by parts with each part separated by a {@code '/'} or
   * {@code null} for none.
   */
  public List<String> getPathParts() {
    return pathParts;
  }

  /**
   * Sets the decoded path component by parts with each part separated by a {@code '/'} or
   * {@code null} for none.
   *
   * <p>
   * For example {@code "/m8/feeds/contacts/default/full"} is represented by {@code "", "m8",
   *"feeds", "contacts", "default", "full"}.
   * </p>
   *
   * <p>
   * Use {@link #appendRawPath(String)} to append to the path, which ensures that no extra slash is
   * added.
   * </p>
   */
  public void setPathParts(List<String> pathParts) {
    this.pathParts = pathParts;
  }

  /**
   * Returns the fragment component or {@code null} for none.
   */
  public String getFragment() {
    return fragment;
  }

  /**
   * Sets the fragment component or {@code null} for none.
   */
  public void setFragment(String fragment) {
    this.fragment = fragment;
  }

  /**
   * Constructs the string representation of the URL, including the path specified by
   * {@link #pathParts} and the query parameters specified by this generic URL.
   */
  public String build() {
    return buildAuthority() + buildRelativeUrl();
  }

  /**
   * Constructs the portion of the URL containing the scheme, host and port.
   *
   * <p>
   * For the URL {@code "http://example.com/something?action=add"} this method would return
   * {@code "http://example.com"}.
   * </p>
   *
   * @return scheme://[user-info@]host[:port]
   */
  public String buildAuthority() {
    // scheme, [user info], host, [port]
    StringBuilder buf = new StringBuilder();
    buf.append((scheme));
    buf.append('://');
    if (userInfo != null) {
      buf.append(/*CharEscapers.escapeUriUserInfo*/(userInfo)).append('@');
    }
    buf.append((host));
    integer port = this.port;
    if (port != -1) {
      buf.append(':').append(port);
    }
    return buf.toString();
  }

  /**
   * Constructs the portion of the URL beginning at the rooted path.
   *
   * <p>
   * For the URL {@code "http://example.com/something?action=add"} this method would return
   * {@code "/something?action=add"}.
   * </p>
   *
   * @return path with with leading '/' if the path is non-empty, query parameters and fragment
   * @since 1.9
   */
  public String buildRelativeUrl() {
    StringBuilder buf = new StringBuilder();
    if (pathParts != null) {
      appendRawPathFromParts(buf);
    }
      
    System.debug('---Adding query parameters');
    System.debug(this.m_FieldValues);  
    addQueryParams(this.m_FieldValues, buf);
    
    // URL fragment
    String fragment = this.fragment;
    if (String.isNotBlank(fragment)) {
      buf.append('#').append(/*URI_FRAGMENT_ESCAPER.escape*/(fragment));
    }
    return buf.toString();
  }

  /**
   * Constructs the URI based on the string representation of the URL from {@link #build()}.
   *
   * <p>
   * Any {@link URISyntaxException} is wrapped in an {@link IllegalArgumentException}.
   * </p>
   *
   * @return new URI instance
   *
   * @since 1.14
   */
  /*
   public  URI toURI() {
    return toURI(build());
  }
  */

  /**
   * Constructs the URL based on the string representation of the URL from {@link #build()}.
   *
   * <p>
   * Any {@link MalformedURLException} is wrapped in an {@link IllegalArgumentException}.
   * </p>
   *
   * @return new URL instance
   *
   * @since 1.14
   */
  public  URL toURL() {
    return parseURL(build());
  }

  /**
   * Constructs the URL based on {@link URL#URL(URL, String)} with this URL representation from
   * {@link #toURL()} and a relative url.
   *
   * <p>
   * Any {@link MalformedURLException} is wrapped in an {@link IllegalArgumentException}.
   * </p>
   *
   * @return new URL instance
   *
   * @since 1.14
   */
  public URL toURL(String relativeUrl) {
    //try {
      URL url = toURL();
      return new URL(url, relativeUrl);
    //} catch (MalformedURLException e) {
    //  throw new IllegalArgumentException(e);
    //}
  }

  /**
   * Returns the first query parameter value for the given query parameter name.
   *
   * @param name query parameter name
   * @return first query parameter value
   */
  public Object getFirst(String name) {
    Object value = get(name);
    /*
    if (value instanceof Collection<?>) {
      @SuppressWarnings('unchecked')
      Collection<Object> collectionValue = (Collection<Object>) value;
      Iterator<Object> iterator = collectionValue.iterator();
      return iterator.hasNext() ? iterator.next() : null;
    }
    */
    return value;
  }

  /**
   * Returns all query parameter values for the given query parameter name.
   *
   * @param name query parameter name
   * @return unmodifiable collection of query parameter values (possibly empty)
   */
  public List<Object> getAll(String name) {
    Object value = get(name);
    if (value == null) {
      return new List<Object>();
    }

    List<Object> l=(List<Object>)value;
    if(null==l){
        l = new List<Object>();
        l.add(value);
    }
    
    return l;
    
    //if (value instanceof Collection<?>) {
    //  @SuppressWarnings("unchecked")
    //  Collection<Object> collectionValue = (Collection<Object>) value;
    //  return Collections.unmodifiableCollection(collectionValue);
    //}
    //return Collections.singleton(value);
  }

  /**
   * Returns the raw encoded path computed from the {@link #pathParts}.
   *
   * @return raw encoded path computed from the {@link #pathParts} or {@code null} if
   *         {@link #pathParts} is {@code null}
   */
  public String getRawPath() {
    List<String> pathParts = this.pathParts;
    if (pathParts == null) {
      return null;
    }
    StringBuilder buf = new StringBuilder();
    appendRawPathFromParts(buf);
    return buf.toString();
  }

  /**
   * Sets the {@link #pathParts} from the given raw encoded path.
   *
   * @param encodedPath raw encoded path or {@code null} to set {@link #pathParts} to {@code null}
   */
  public void setRawPath(String encodedPath) {
    pathParts = toPathParts(encodedPath);
  }

  /**
   * Appends the given raw encoded path to the current {@link #pathParts}, setting field only if it
   * is {@code null} or empty.
   * <p>
   * The last part of the {@link #pathParts} is merged with the first part of the path parts
   * computed from the given encoded path. Thus, if the current raw encoded path is {@code "a"}, and
   * the given encoded path is {@code "b"}, then the resulting raw encoded path is {@code "ab"}.
   * </p>
   *
   * @param encodedPath raw encoded path or {@code null} to ignore
   */
  public void appendRawPath(String encodedPath) {
    if (encodedPath != null && encodedPath.length() != 0) {
      List<String> appendedPathParts = toPathParts(encodedPath);
      if (pathParts == null || pathParts.isEmpty()) {
        this.pathParts = appendedPathParts;
      } else {
        integer size = pathParts.size();
        pathParts.set(size - 1, pathParts.get(size - 1) + appendedPathParts.get(0));

        List<String> sublist=new List<String>();
        for(integer idx=1;idx<appendedPathParts.size();idx++)
            sublist.Add(appendedPathParts[idx]);
        //pathParts.addAll(appendedPathParts.subList(1, appendedPathParts.size()));
      }
    }
  }

  /**
   * Returns the decoded path parts for the given encoded path.
   *
   * @param encodedPath slash-prefixed encoded path, for example
   *        {@code "/m8/feeds/contacts/default/full"}
   * @return decoded path parts, with each part assumed to be preceded by a {@code '/'}, for example
   *         {@code "", "m8", "feeds", "contacts", "default", "full"}, or {@code null} for
   *         {@code null} or {@code ""} input
   */
  public static List<String> toPathParts(String encodedPath) {
    if (encodedPath == null || encodedPath.length() == 0) {
      return null;
    }
    List<String> result = new List<String>();
    integer cur = 0;
    boolean notDone = true;
    while (notDone) {
      integer slash = encodedPath.indexOf('/', cur);
      notDone = slash != -1;
      String sub;
      if (notDone) {
        sub = encodedPath.substring(cur, slash);
      } else {
        sub = encodedPath.substring(cur);
      }
      result.add(/*CharEscapers.decodeUri*/(sub));
      cur = slash + 1;
    }
    return result;
  }

  private void appendRawPathFromParts(StringBuilder buf) {
    integer size = pathParts.size();
    for (integer i = 0; i < size; i++) {
      String pathPart = pathParts.get(i);
      System.debug('---part: '+pathPart);
      if (i != 0) {
        buf.append('/');
      }
      if (pathPart.length() != 0) {
        buf.append(/*CharEscapers.escapeUriPath*/(pathPart));
      }
    }
  }

  /**
   * Adds query parameters from the provided entrySet into the buffer.
   */
  static void addQueryParams(Map<String, Object> entrySet, StringBuilder buf) {
    // (similar to UrlEncodedContent)
    boolean first = true;
    for (string name : entrySet.keySet()) {
        //CharEscapers.escapeUriQuery
      Object value = entrySet.get(name);
      if (value != null) {
        String strValue = /*CharEscapers.escapeUriQuery*/String.valueOf(value);
        if (value instanceof List<Object>) {
          List<Object>  collectionValue = (List<Object>) value;
          for (Object repeatedValue : collectionValue) {
            first = appendParam(first, buf, name, repeatedValue);
          }
        } else {
          first = appendParam(first, buf, name, value);
        }
      }
    }
  }

  private static boolean appendParam(boolean first, StringBuilder buf, String name, Object value) {
    if (first) {
      first = false;
      buf.append('?');
    } else {
      buf.append('&');
    }
    buf.append(name);
    String stringValue = /*CharEscapers.escapeUriQuery*/(value.toString());
      System.debug('|'+stringValue+'|');
    if (stringValue.length() != 0) {
      buf.append('=').append(stringValue);
    }
    return first;
  }

  /**
   * Returns the URI for the given encoded URL.
   *
   * <p>
   * Any {@link URISyntaxException} is wrapped in an {@link IllegalArgumentException}.
   * </p>
   *
   * @param encodedUrl encoded URL
   * @return URI
   */
  /*
  private static URI toURI(String encodedUrl) {
    try {
      return new URI(encodedUrl);
    } catch (URISyntaxException e) {
      throw new IllegalArgumentException(e);
    }
  }
*/

  /**
   * Returns the URI for the given encoded URL.
   *
   * <p>
   * Any {@link MalformedURLException} is wrapped in an {@link IllegalArgumentException}.
   * </p>
   *
   * @param encodedUrl encoded URL
   * @return URL
   */
  private static URL parseURL(String encodedUrl) {
    //try {
      return new URL(encodedUrl);
    //} 
    /*
    catch (MalformedURLException e) {
      throw new IllegalArgumentException(e);
    }
    */
  }
}