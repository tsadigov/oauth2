/**
 * OAuth 2.0 JSON model for a successful access token response as specified in <a
 * href="http://tools.ietf.org/html/rfc6749#section-5.1">Successful Response</a>.
 *
 *
 * @author Tural SADIK
 */
public class TokenResponse {//extends GenericJson {

    public String error{get;set;}
    public string description{get;set;}
  /** Access token issued by the authorization server. */
  public String access_token;

  /**
   * Token type (as specified in <a href="http://tools.ietf.org/html/rfc6749#section-7.1">Access
   * Token Types</a>).
   */
  //@Key("token_type")
  public String token_type;

  /**
   * Lifetime in seconds of the access token (for example 3600 for an hour) or {@code null} for
   * none.
   * 
   * You dont know when it was issued
   */
  public Long expires_in;

  /**
   * Refresh token which can be used to obtain new access tokens using {@link RefreshTokenRequest}
   * or {@code null} for none.
   */
  //@Key("refresh_token")
  public String refresh_token;

  /**
   * Scope of the access token as specified in <a
   * href="http://tools.ietf.org/html/rfc6749#section-3.3">Access Token Scope</a> or {@code null}
   * for none.
   */
  //@Key
  public String scope;

  public boolean IsOk{get{return String.isNotBlank(this.access_token);}}

}